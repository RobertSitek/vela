package com.star.vela.user.model;

public enum Gender {
    MALE, FEMALE, OTHER
}
