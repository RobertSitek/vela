package com.star.vela.user.service;

import com.star.vela.user.model.User;
import com.star.vela.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public List<User> getUsers() {
        return userRepository.findAll();
    }


    public void addUser(User user) {

    }
}
