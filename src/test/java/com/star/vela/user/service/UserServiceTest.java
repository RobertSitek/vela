package com.star.vela.user.service;

import com.star.vela.user.model.Gender;
import com.star.vela.user.model.User;
import com.star.vela.user.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    private User user;

    @Test
    void getUsers() {

    }

    @Test
    void userShouldInsertedToDatabase() {
        user = new User(1L,
                "Paul",
                "Doe",
                "pdoe",
                "paul@gmail.com",
                "password",
                Gender.MALE,
                LocalDate.now());
        given(userRepository.save(user));
    }
}